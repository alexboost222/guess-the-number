﻿using GuessTheNumber.Core;
using TMPro;
using UniRx;
using UnityEngine;

namespace GuessTheNumber.GameView
{
    [RequireComponent(typeof(TMP_Text))]
    public class LastChosenNumberView : MonoBehaviour
    {
        [SerializeField] private GameController gameController;
        private TMP_Text text;

        private void Awake()
        {
            text = GetComponent<TMP_Text>();
            gameController.LastChosenNumber.Subscribe(OnLastChosenNumberChanged).AddTo(this);
        }

        private void OnLastChosenNumberChanged(int? number)
        {
            text.text = number == null ? "-" : $"{number.Value}";
        }
    }
}