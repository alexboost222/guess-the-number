﻿using GuessTheNumber.Core;
using TMPro;
using UnityEngine;

namespace GuessTheNumber.GameView
{
    [RequireComponent(typeof(TMP_Text))]
    public class WinnerView : MonoBehaviour
    {
        [SerializeField] private GameController gameController;

        private TMP_Text _text;

        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
        }

        private void Update()
        {
            _text.text = gameController.Winner == null ? "-" : gameController.Winner.ToString();
        }
    }
}