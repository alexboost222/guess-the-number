﻿using GuessTheNumber.Core;
using TMPro;
using UnityEngine;

namespace GuessTheNumber.GameView
{
    [RequireComponent(typeof(TMP_Text))]
    public class LastAnswerResultView : MonoBehaviour
    {
        [SerializeField] private GameController gameController;
        private TMP_Text text;

        private void Awake()
        {
            text = GetComponent<TMP_Text>();
        }

        private void Update()
        {
            if (gameController.AnswerResult == null)
                text.text = "-";
            else
                text.text = gameController.AnswerResult.ToString();
        }
    }
}