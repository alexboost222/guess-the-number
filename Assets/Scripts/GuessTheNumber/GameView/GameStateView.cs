﻿using GuessTheNumber.Core;
using TMPro;
using UniRx;
using UnityEngine;

namespace GuessTheNumber.GameView
{
    [RequireComponent(typeof(TMP_Text))]
    public class GameStateView : MonoBehaviour
    {
        [SerializeField] private GameController gameController;

        private TMP_Text _text;

        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
            gameController.GameState.Subscribe(OnGameStateChanged).AddTo(this);
        }

        private void OnGameStateChanged(EGameState gameState)
        {
            _text.text = gameState.ToString();
        }
    }
}