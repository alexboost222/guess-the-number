﻿using GuessTheNumber.Core;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace GuessTheNumber.Controls
{
    [RequireComponent(typeof(Button))]
    public class NextGameButton : MonoBehaviour
    {
        [SerializeField] private GameController gameController;

        private void Awake()
        {
            var button = GetComponent<Button>();
            button.onClick.AsObservable()
                .Subscribe(_ => gameController.ContinueFlag = true)
                .AddTo(this);
            gameController.GameState
                .Subscribe(gameState => button.gameObject.SetActive(gameState == EGameState.Finished))
                .AddTo(this);
        }
    }
}