﻿using System;
using GuessTheNumber.Core;
using GuessTheNumber.KeyboardInput;
using UniRx;
using UnityEngine;

namespace GuessTheNumber.Controls
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] private GameController gameController;
        [SerializeField] private Keyboard keyboard;

        private IDisposable _keyboardSubscription;

        private void Awake()
        {
            gameController.GameState.Subscribe(OnGameStateValueChanged).AddTo(this);
        }

        private void OnGameStateValueChanged(EGameState gameState)
        {
            if (gameState != EGameState.PlayerChoice)
            {
                _keyboardSubscription?.Dispose();
                _keyboardSubscription = null;
                keyboard.enabled = false;
            }
            else
            {
                _keyboardSubscription = keyboard.NumberChosen.Subscribe(OnNumberChosen);
                keyboard.enabled = true;
            }
        }

        private void OnNumberChosen(int number)
        {
            Debug.Log($"Player Chosen {number}");
            gameController.PlayerNumber = number;
        }
    }
}