﻿using System;
using Cysharp.Threading.Tasks;
using GuessTheNumber.Core;
using UniRx;
using UnityEngine;
using Random = System.Random;

namespace GuessTheNumber.Controls
{
    public class AIInput : MonoBehaviour
    {
        [SerializeField] private int minWaitTime;
        [SerializeField] private int maxWaitTime;
        [SerializeField] private GameController gameController;

        private readonly Random _random = new(Guid.NewGuid().GetHashCode());

        private void Awake()
        {
            gameController.GameState.Subscribe(OnGameStateValueChanged).AddTo(this);
        }

        private void OnGameStateValueChanged(EGameState gameState)
        {
            if (gameState == EGameState.AIChoice)
            {
                WaitAndChooseRandomNumberAsync().Forget();
            }
        }

        private async UniTaskVoid WaitAndChooseRandomNumberAsync()
        {
            await UniTask.WaitForSeconds(_random.Next(minWaitTime, maxWaitTime));
            int number = _random.Next(gameController.MinNumber, gameController.MaxNumber);
            Debug.Log($"AI Chosen {number}");
            gameController.AINumber = number;
        }
    }
}