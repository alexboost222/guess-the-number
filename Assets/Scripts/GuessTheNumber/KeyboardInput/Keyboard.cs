﻿using System;
using UniRx;
using UnityEngine;

namespace GuessTheNumber.KeyboardInput
{
    public class Keyboard : MonoBehaviour
    {
        private readonly Subject<int> _numberChosen = new();

        public IObservable<int> NumberChosen => _numberChosen;
        public int CurrentNumber { get; private set; }

        public void AddNumber(int number)
        {
            if (number is < 0 or > 9)
                throw new Exception($"Keyboard not support number {number}. Number must be between [0, 9]");
            CurrentNumber = CurrentNumber * 10 + number;
        }

        public void Backspace()
        {
            CurrentNumber /= 10;
        }

        public void Submit()
        {
            _numberChosen.OnNext(CurrentNumber);
        }

        private void Awake()
        {
            _numberChosen.AddTo(this);
        }
    }
}