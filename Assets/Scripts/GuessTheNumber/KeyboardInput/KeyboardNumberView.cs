﻿using TMPro;
using UnityEngine;

namespace GuessTheNumber.KeyboardInput
{
    [RequireComponent(typeof(TMP_Text))]
    public class KeyboardNumberView : MonoBehaviour
    {
        [SerializeField] private Keyboard keyboard;
        private TMP_Text text;

        private void Awake()
        {
            text = GetComponent<TMP_Text>();
        }

        private void Update()
        {
            text.text = keyboard.CurrentNumber.ToString();
        }
    }
}