﻿using TMPro;
using UnityEngine;

namespace GuessTheNumber.KeyboardInput
{
    [RequireComponent(typeof(TMP_Text))]
    public class NumberKeyboardButtonView : MonoBehaviour
    {
        [SerializeField] private NumberKeyboardButton button;

        private void Awake()
        {
            GetComponent<TMP_Text>().text = button.Number.ToString();
        }
    }
}