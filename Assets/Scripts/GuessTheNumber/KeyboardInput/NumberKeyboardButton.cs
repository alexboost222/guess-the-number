﻿using UnityEngine;

namespace GuessTheNumber.KeyboardInput
{
    public class NumberKeyboardButton : KeyboardButton
    {
        [field: SerializeField, Range(0, 9)] public int Number { get; private set; }

        protected override void OnClick()
        {
            keyboard.AddNumber(Number);
        }
    }
}