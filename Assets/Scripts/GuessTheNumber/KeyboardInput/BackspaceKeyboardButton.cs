﻿namespace GuessTheNumber.KeyboardInput
{
    public class BackspaceKeyboardButton : KeyboardButton
    {
        protected override void OnClick()
        {
            keyboard.Backspace();
        }
    }
}