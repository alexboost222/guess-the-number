﻿namespace GuessTheNumber.KeyboardInput
{
    public class SubmitKeyboardButton : KeyboardButton
    {
        protected override void OnClick()
        {
            keyboard.Submit();
        }
    }
}