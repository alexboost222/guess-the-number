﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace GuessTheNumber.KeyboardInput
{
    [RequireComponent(typeof(Button))]
    public abstract class KeyboardButton : MonoBehaviour
    {
        [SerializeField] protected Keyboard keyboard;

        protected abstract void OnClick();

        private void Awake()
        {
            var button = GetComponent<Button>();
            button.onClick.AsObservable()
                .Subscribe(_ => OnClick()).AddTo(this);
            Observable.EveryUpdate()
                .Subscribe(_ => button.interactable = keyboard.enabled)
                .AddTo(this);
        }
    }
}