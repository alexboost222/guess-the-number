﻿using System;
using UniRx;
using UnityEngine;
using Random = System.Random;

namespace GuessTheNumber.Core
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private int minNumber;
        [SerializeField] private int maxNumber;

        private readonly Random _random = new(Guid.NewGuid().GetHashCode());
        private int? _playerNumber;
        private int? _aiNumber;

        private readonly ReactiveProperty<int> _currentNumber = new();
        public IReadOnlyReactiveProperty<int> CurrentNumber => _currentNumber;

        private readonly ReactiveProperty<EGameState> _gameState = new();
        public IReadOnlyReactiveProperty<EGameState> GameState => _gameState;

        private readonly ReactiveProperty<int?> _lastChosenNumber = new();
        public IReadOnlyReactiveProperty<int?> LastChosenNumber => _lastChosenNumber;

        public int? PlayerNumber
        {
            get => _playerNumber;
            set => _playerNumber = value;
        }

        public int? AINumber
        {
            get => _aiNumber;
            set => _aiNumber = value;
        }

        public bool ContinueFlag { get; set; }

        public int MinNumber => minNumber;
        public int MaxNumber => maxNumber;

        public EAnswerResult? AnswerResult { get; private set; }
        public EPlayerTag? Winner { get; private set; }

        private void Awake()
        {
            _currentNumber.AddTo(this);
            _gameState.AddTo(this);
            _lastChosenNumber.AddTo(this);

            _currentNumber.Subscribe(LogCurrentNumber).AddTo(this);
            _gameState.Subscribe(LogGameState).AddTo(this);
        }

        private void LogCurrentNumber(int currentNumber)
        {
            Debug.Log($"Current number {currentNumber}");
        }

        private void LogGameState(EGameState gameState)
        {
            Debug.Log($"Current state {gameState}");
        }

        private void Update()
        {
            switch (_gameState.Value)
            {
                case EGameState.SetTheNumber:
                    _currentNumber.Value = _random.Next(minNumber, maxNumber);
                    _gameState.Value = EGameState.PlayerChoice;
                    Winner = null;
                    break;
                case EGameState.PlayerChoice:
                    if (TrySetAnswerResultAndNextState(in _playerNumber, EGameState.AIChoice, EPlayerTag.Player))
                    {
                        _lastChosenNumber.Value = _playerNumber!.Value;
                        _playerNumber = null;
                    }
                    break;
                case EGameState.AIChoice:
                    if (TrySetAnswerResultAndNextState(in _aiNumber, EGameState.PlayerChoice, EPlayerTag.AI))
                    {
                        _lastChosenNumber.Value = _aiNumber!.Value;
                        _aiNumber = null;
                    }
                    break;
                case EGameState.Finished:
                    if (!ContinueFlag)
                        break;
                    ContinueFlag = false;
                    _gameState.Value = EGameState.SetTheNumber;
                    AnswerResult = null;
                    _lastChosenNumber.Value = null;
                    break;
            }
        }

        private bool TrySetAnswerResultAndNextState(in int? number, EGameState nextStateIfNotFinished, EPlayerTag player)
        {
            if (number == null)
                return false;
            if (number.Value > _currentNumber.Value)
            {
                AnswerResult = EAnswerResult.Greater;
                _gameState.Value = nextStateIfNotFinished;
            }
            else if (number.Value < _currentNumber.Value)
            {
                AnswerResult = EAnswerResult.Lower;
                _gameState.Value = nextStateIfNotFinished;
            }
            else
            {
                AnswerResult = EAnswerResult.Equal;
                _gameState.Value = EGameState.Finished;
                Winner = player;
            }
            return true;
        }
    }
}

