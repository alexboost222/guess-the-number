﻿namespace GuessTheNumber.Core
{
    public enum EAnswerResult
    {
        Lower = 0,
        Equal = 1,
        Greater = 2
    }
}