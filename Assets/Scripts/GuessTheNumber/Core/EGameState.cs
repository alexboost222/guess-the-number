﻿namespace GuessTheNumber.Core
{
    public enum EGameState
    {
        SetTheNumber = 0,
        PlayerChoice = 1,
        AIChoice = 2,
        Finished = 3
    }
}