﻿namespace GuessTheNumber.Core
{
    public enum EPlayerTag
    {
        Player = 0,
        AI = 1
    }
}